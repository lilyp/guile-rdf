;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (rdf xsd)
  #:use-module (ice-9 match)
  #:use-module (rdf rdf)
  #:export (datatypes order compatible?))

;; This module implements the xsd datatypes, as presented in https://www.w3.org/TR/rdf11-concepts/#xsd-datatypes

(define (make-xsd-datatype iri description lexical? value? lexical->value
                           value->lexical)
  (make-rdf-datatype
    (list (string-append "http://www.w3.org/2001/XMLSchema#" iri))
    description
    lexical?
    value?
    lexical->value
    value->lexical))

(define-public string
  (make-xsd-datatype
    "string"
    "Character strings (but not all Unicode character strings)"
    string?
    string?
    identity
    identity))

(define-public boolean
  (make-xsd-datatype
    "boolean"
    "true, false"
    (lambda (lexical)
      (member lexical '("true" "false" "0" "1")))
    boolean?
    (lambda (lexical)
      (match lexical
        ("true" #t)
        ("1" #t)
        ("false" #f)
        ("0" #f)))
    (lambda (value)
      (match value
        (#t "true")
        (#f "false")))))

(define-public decimal
  (make-xsd-datatype
    "decimal"
    "Arbitrary-precision decimal numbers"
    (lambda (lexical)
      (and (null? (filter (lambda (c)
			    (or (eqv? c #\e)
				(eqv? c #\E)))
			  (string->list lexical)))
	   (string->number lexical)))
    number?
    string->number
    number->string))

(define-public integer
  (make-xsd-datatype
    "integer"
    "Arbitrary-size integer numbers"
    (lambda (lexical)
      (integer? (string->number lexical)))
    integer?
    string->number
    number->string))

(define-public int
  (make-xsd-datatype
    "int"
    "Limited-range integer numbre (32 bits)"
    (lambda (lexical)
      (and (integer? (string->number lexical))
           (>= (string->number lexical) -2147483648)
           (<= (string->number lexical) 2147483647)))
    (lambda (value)
      (and (integer? value)
           (>= value 2147483648)
           (<= value 2147483647)))
    string->number
    number->string))

(define-public double
  (make-xsd-datatype
    "double"
    "The double datatype is patterned after the IEEE double-precision 64-bit
floating point datatype.  Each floating point datatype has a value space that
is a subset of the rational numbers."
    string->number
    number?
    string->number
    number->string))

(define datatypes
  (list string boolean decimal integer int))

(define sub-classes
  (list
    (list rdf:langString)
    (list string)
    (list boolean)
    (list decimal integer int)
    (list integer int)
    (list int)))

(define (order d1 d2)
  "Return whether d1's value space is included in d2's"
  (member d1 (assoc-ref sub-classes d2)))

;; TODO: this is not entirely correct
(define (compatible? d1 d2)
  (or (order d1 d2) (order d2 d1)))
