;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (rdf entailment rdf)
  #:use-module (ice-9 match)
  #:use-module (rdf rdf)
  #:use-module ((rdf entailment d) #:prefix d:)
  #:use-module (rdf utils)
  #:use-module (srfi srfi-1)
  #:export (consistent-graph?
            entails?))

(define (rdf-iri name)
  (string-append "http://www.w3.org/1999/02/22-rdf-syntax-ns#" name))

(define (consistent-graph? graph vocabulary)
  (define (compatible? types)
    (match (filter rdf-datatype? types)
      (() #t)
      ((_) #t)
      ((a b ...)
       (and (null? (filter
                     (lambda (t)
                       (not ((rdf-vocabulary-compatible? vocabulary) a t)))
                     b))
            (compatible? b)))))

  (define (compatible-types? graph)
    (let loop ((graph graph) (type-mappings '()))
      (if (null? graph)
          (null?
            (filter
              (lambda (t)
                (not (compatible? (cdr t))))
              type-mappings))
          (let* ((t (car graph)))
            (if (equal? (rdf-triple-predicate t) (rdf-iri "type"))
                (loop
                  (cdr graph)
                  (alist-set type-mappings (rdf-triple-subject t)
                             (cons (rdf-triple-object t)
                                   (or
                                     (assoc-ref type-mappings
                                                (rdf-triple-subject t))
                                     '()))))
                (loop (cdr graph) type-mappings))))))

  (and (d:consistent-graph? graph vocabulary)
       (compatible-types? (augment (recognize graph vocabulary) vocabulary))))

;; G entails E if E has an instance (where blank nodes are replaced by literals
;; or IRIs) that is a subgraph of G.
;;
;; We re-use similar procedures to verifying isomorphism of graphs, but this time
;; blank nodes can also map to literals and IRIs.

;; We follow appendix A and use a subgraph comparison (like the simple:entails?
;; procedure) after augmenting the graph with additional true triples.

(define rdf-axioms
  (list
    (make-rdf-triple (rdf-iri "type") (rdf-iri "type") (rdf-iri "Property"))
    (make-rdf-triple (rdf-iri "subject") (rdf-iri "type") (rdf-iri "Property"))
    (make-rdf-triple (rdf-iri "predicate") (rdf-iri "type") (rdf-iri "Property"))
    (make-rdf-triple (rdf-iri "object") (rdf-iri "type") (rdf-iri "Property"))
    (make-rdf-triple (rdf-iri "first") (rdf-iri "type") (rdf-iri "Property"))
    (make-rdf-triple (rdf-iri "rest") (rdf-iri "type") (rdf-iri "Property"))
    (make-rdf-triple (rdf-iri "value") (rdf-iri "type") (rdf-iri "Property"))
    (make-rdf-triple (rdf-iri "nil") (rdf-iri "type") (rdf-iri "List"))))

(define (rdf-axioms-container container)
  (list
    (make-rdf-triple 
      container (rdf-iri "type") (rdf-iri "Property"))))

(define (rdf-container-property? p)
  (define rdf-container-property-base (rdf-iri "_"))
  (and (string? p)
       (> (string-length p) (string-length rdf-container-property-base))
       (equal? (substring p 0 (string-length rdf-container-property-base))
               rdf-container-property-base)
       (string->number
         (substring p (string-length rdf-container-property-base)))))

(define (rdf-container-properties g)
  (let loop ((answer '()) (g g))
    (match g
      (() (if (null? answer) (list (rdf-iri "_1")) answer))
      ((($ rdf-triple subject predicate object) g ...)
       (let* ((answer (if (and (rdf-container-property? subject)
                               (not (member subject answer)))
                          (cons subject answer)
                          answer))
              (answer (if (and (rdf-container-property? predicate)
                               (not (member predicate answer)))
                          (cons predicate answer)
                          answer))
              (answer (if (and (rdf-container-property? object)
                               (not (member object answer)))
                          (cons object answer)
                          answer)))
         (loop answer g))))))

(define (augment g vocabulary)
  (let* ((g (append rdf-axioms g))
         (g (append
              (append-map rdf-axioms-container (rdf-container-properties g))
              g)))
    (let loop ((g (recognize g vocabulary)))
      (let ((augment-set
             (let loop2 ((g2 g) (augment-set '()))
               (match g2
                 (() augment-set)
                 ((($ rdf-triple subject predicate object) g2 ...)
                  (let ((type-triple
                          (if (and (rdf-literal? object)
                                   (rdf-datatype? (rdf-literal-type object)))
                            (make-rdf-triple object (rdf-iri "type")
                                             (rdf-literal-type object))
                            #f))
                        (property-triple
                          (make-rdf-triple predicate (rdf-iri "type")
                                           (rdf-iri "Property"))))
                  (loop2
                    g2
                    (append
                      (if (or (not type-triple) (member type-triple g)
                              (member type-triple augment-set))
                          '()
                          (list type-triple))
                      (if (or (member property-triple g)
                              (member type-triple augment-set))
                          '()
                          (list property-triple))
                      augment-set))))))))
        (if (null? augment-set)
            g
            (loop (append (recognize augment-set vocabulary) g)))))))

(define (entails? g e vocabulary)
  "Return true if g entails e"
  (let* ((g (recognize g vocabulary))
         (g (augment g vocabulary))
         (e (recognize e vocabulary)))
    (or (not (consistent-graph? g vocabulary))
        (d:entails? g e vocabulary))))
