;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;;
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;;
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;;

(define-module (test-modules download-tests)
  #:use-module (ice-9 match)
  #:use-module (rnrs bytevectors)
  #:use-module (test-modules test-case)
  #:use-module (test-modules testsuite)
  #:use-module (turtle tordf)
  #:use-module (web client)
  #:use-module (web response)
  #:export (download-test-files))

(define test-dir "test-files")

(define base-url "http://www.w3.org/2013/")

(define (get-test-doc url)
  "Get a test suite object from the manifest at @var{url}."
  (call-with-values
    (lambda ()
      (http-get url))
    (lambda (hdr body)
      (if (equal? (response-code hdr) 200)
          (if (string? body)
              body
              (utf8->string body))
          (throw 'error-fetching-test-manifest (response-code hdr))))))

(define (mkdir-p dir)
  (unless (file-exists? dir)
    (mkdir-p (dirname dir))
    (mkdir dir)))

(define (download url)
  (let ((filename (string-append test-dir "/"
                                 (substring url (string-length base-url)))))
    (mkdir-p (dirname filename))
    (call-with-output-file filename
      (lambda (port)
        (display (get-test-doc url) port)))))

(define (download-test-files)
  (for-each
    (lambda (manifest)
      (pk 'downloading manifest)
      (download manifest)
      (let ((plan (rdf->test-plan
                    (turtle->rdf (get-test-doc manifest) manifest)
                    manifest)))
        (for-each
          (lambda (test)
            (download (test-case-action test))
            (when (test-case-result test)
              (download (test-case-result test))))
          plan)))
    manifests))

(download-test-files)
